import express from "express";
import compression from "compression";
import index from "./src/routes/index";
import path from "path";
import session from 'express-session';
import mongoose from 'mongoose';
import mongo_connect from 'connect-mongo';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import passport from 'passport';
import csrf from 'csurf';
import dotenv from 'dotenv';
import cors from 'cors';

dotenv.config()

const {
  HOST,
  DATABASE,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
} = process.env;

const url = `mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${HOST}/${DATABASE}?retryWrites=true&w=majority`;

mongoose.connect(url, { useNewUrlParser: true })
mongoose.connection.on('error', (err) => {
  console.error(`connecting to mongodb failed: ${err.message}`);
});


const MongoStore = mongo_connect(session);


// Server var
const app = express();

// View engine setup
app.set("views", path.join(__dirname, "src", "views"));
app.set("view engine", "ejs");

// Middleware
app.use(compression());
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(cookieParser());

// secret ist used to sign the session ID cookie
// key ist the name for the signature, like key: xxxxxx


app.use(session({
    secret: process.env.SECRET,
    key: process.env.KEY,
    resave: false,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(csrf());

app.use((req, res, next) => {
  res.locals.csrftoken = req.csrfToken();
  //res.locals.user = req.user || null;
  res.locals.currentPath = req.path;
  next();
});



const corsOptions = {origin: 'origin'}
// using cors(), the response headers will contain 'Access-Control-Allow-Origin'
app.use(cors(corsOptions));

//Routes
app.use("/", index);

const port = process.env.PORT || 8080;

app.listen(port, function listenHandler() {
    console.info(`Running on ${port}`)
});
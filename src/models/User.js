import mongoose, {Schema} from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';

const userSchema = new Schema({
    username: {
      type: String,
      unique: true,
      required: 'Please Supply a name!',
      trim: true,
    }
  });
  
  userSchema.plugin(passportLocalMongoose, { usernameField: 'username' });
  
  export default mongoose.model('User', userSchema);
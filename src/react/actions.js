/*
Redux actions
*/

import {ACTIONS} from './constants';

export function fetchSlides(name) {
    return {
        type: ACTIONS.FETCH_SLIDES,
        payload: {
            typ: name
        }
    };
}

export function modifySlide(id, index) {
    return {
        type: ACTIONS.MODIFY_SLIDE,
        payload: {
            edittingId: id,
            edittingIndex: index
        }
    };
}

export function deleteSlide(id, index) {
    return {
        type: ACTIONS.DELETE_SLIDE,
        payload: {
            edittingId: id,
            edittingIndex: index
        }
    };
}

export function addSlide(index) {
    return {
        type: ACTIONS.ADD_SLIDE,
        payload: {
            edittingIndex: index
        }
    };
}

export function cancelEditting() {
    return {
        type: ACTIONS.CANCEL_EDITTING
    };
}



/*
Those functions help to get and post data to backend with csrf-Token.
*/

function checkStatus(response) {
    if(response.ok) {
        return response;
    } else {
        const error = new Error(response.statusText);
        throw error;
    }
}

const cookieDict = {};

//get csrf token from document.cookie
function getCookie(name) {
    if(cookieDict[name]) {
        return cookieDict[name];
    }

    const reg = new RegExp(name + "=[^;]+");
    const value = reg.exec(document.cookie);

    if(value) {
        cookieDict[name] = value[1];
    }
    return cookieDict[name];
}

export function fetchAndCheck(url, options = {}) {
    Object.assign(options, {credentials: 'same-origin'});
    if (!options.headers) options.headers = {};
    options.headers['CSRF-Token'] = getCookie("csrfToken")

    return fetch(url, options)
        .catch(err => {
            throw new NetworkError(err.messge, err);
        })
        .then(checkStatus);
} 

export function getData(url) {
    const options = {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    };
    return fetchAndCheck(url, options).then(response => response.json());
}

export function postData(url, data) {
    const options = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };
    return fetchAndCheck(url, options).then(res => {
        return res.text().then(text => {
            return text ? Promise.resolve(JSON.parse(text)) : Promise.resolve(text);
        })
    })
}


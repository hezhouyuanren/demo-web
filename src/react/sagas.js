import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { ACTIONS } from './constants';
import { getData, postData } from './fetch';

export function* sagasFetchSlides(action) {
    const {typ} = action.payload;;
    const slides = yield call(getData, '/slides/' + typ);
    if (slides) {
        yield put({
            type: ACTIONS.GOT_SLIDES,
            payload: {
                slides: slides,
                typ: typ
            }
        })
        
    }
}

export default function* watcher() {
    yield takeEvery(ACTIONS.FETCH_SLIDES, sagasFetchSlides);
}
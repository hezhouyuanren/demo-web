/*
This component is used in Slideshow presenting the dropdown button 
for operations. In this component, the handle functions for
clicking on operations are with redux-actions defined, which makes 
the store aware of the status.
*/

import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import {useDispatch} from 'react-redux';
import React, {useCallback} from 'react';
import {deleteSlide, modifySlide, addSlide} from '../actions';
import PropTypes from 'prop-types';

const OperationButton = (props) => {
    const dispatch = useDispatch();
    const {slide, index} = props;

    const onDelete = useCallback(() => {
        dispatch(deleteSlide(slide.id, index));
    }, [dispatch, deleteSlide, slide, index]);

    const onModify = useCallback(() => {
        dispatch(modifySlide(slide.id, index));
    }, [dispatch, modifySlide, slide, index]);

    const onAdd = useCallback(() => {
        dispatch(addSlide(index));
    }, [dispatch, addSlide, index]);

    return (
        <DropdownButton title='&#x2027; &#x2027; &#x2027;' variant='warning' size='lg'>
            <Dropdown.Item onClick={onAdd}>Add</Dropdown.Item>
            <Dropdown.Item onClick={onModify}>Modify</Dropdown.Item>
            <Dropdown.Item onClick={onDelete}>Delete</Dropdown.Item>
        </DropdownButton>
    )
}

OperationButton.propTypes = {
    slide: PropTypes.object.isRequired,
    index: PropTypes.any
};

export default OperationButton;
/*
This is a Slideshow with animations. 
*/

import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import OperationButton from './OperationButton';
import OperationModal from './OperationModal';

const SlideShow = (props) => {

    const {slides, edittingIndex, edittingId, edittingTyp} = props;
    const IMAGE_PARTS = 4;
    
    const [activeSlide, setActiveSlide] = useState(-1);
    const [sliderReady, setSliderReady] = useState(false);
    
    const changeSlides = (change) => {
        const length = slides.length;
        let active_slide = activeSlide + change;
        if (active_slide < 0) active_slide = length - 1;
        if (active_slide >= length) active_slide = 0;
        setActiveSlide(active_slide);
    }

    useEffect(() => {
        setActiveSlide(0);
        setSliderReady(true);
    }, [slides]);

    
    return (
        <div className={classNames('slideshow', { 'ready': sliderReady })}>
            <div className="slideshow_slides">
            {slides.map((slide, index) => (
                <div
                className={classNames('slideshow_slide', { 'active': activeSlide === index})}
                key={index}
                >
                <OperationButton slide={slide} index={index}/>
                {(edittingTyp === 'add') ? 
                    <OperationModal slide={slide} typ={edittingTyp} />
                    : ((edittingIndex !== '') && (edittingId === slide.id))?
                        <OperationModal slide={slide} typ={edittingTyp} />
                        : null
                }
                <div className="slideshow_slide__content">
                    <h3 className="slideshow_slide__text">{slide.text}</h3>
                    <h2 className="slideshow_slide__title">
                    {slide.title.split(' ').map((word, index) => <span key={index}>{word}</span>)}
                    </h2>
                    <p className="slideshow_slide__readmore">
                        {slide.link ? (<a href={slide.link}>mehr Info</a>) : "mehr Info"}
                    </p>
                </div>
                <div className="slideshow_slide__parts">
                    {[...Array(IMAGE_PARTS).fill()].map((x, i) => (
                    <div className="slideshow_slide__part" key={i}>
                        <div className="slideshow_slide__part-inner" style={{ backgroundImage: `url(${slide.img})` }} />
                    </div>
                    ))}
                </div>
                </div>
            ))}
            </div>
            <div className="slideshow_control" onClick={() => changeSlides(-1)}>&#10094;</div>
            <div className="slideshow_control slideshow_control__right" onClick={() => changeSlides(1)}>&#10095;</div>
        </div>
    );    
};

SlideShow.propTypes = {
    slides: PropTypes.array.isRequired,
    edittingIndex: PropTypes.any,
    edittingId: PropTypes.any,
    edittingTyp:PropTypes.string
};

SlideShow.defaultProps = {
    edittingIndex: '',
    edittingId: '',
    edittingTyp:''
};

export default SlideShow;

  
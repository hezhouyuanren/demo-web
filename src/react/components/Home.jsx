/*
This Component is responsive to mobil phone. 
 */
import React from 'react';
import {Animal, Tree} from './Container';

const Home = (props) => {
    return(
        <div className='home'>
            <div className='introduction'>
                <Animal />
            </div>
            <div className='mobil-trees-container'>
                <p className="mobil-trees-title">--------------------</p>
                <Tree />
            </div>
        </div>

    );
};

export default Home;
/*
This Component provides two kindes of containers, which can fetch
Slides from backend and render Slideshow Component.
*/ 

import React, { useEffect } from 'react';
import SlideShow from './SlideShow';
import { fetchSlides } from '../actions';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const Container = (props) => {
    const {typ, slides, fetchSlides, ...others} = props;

    useEffect(() => {
      fetchSlides(typ);
    }, [fetchSlides])

    return(
        <div className='slides-container'>
            <SlideShow slides={slides} {...others}/>
        </div>
    );
};

Container.propTypes = {
  typ: PropTypes.string.isRequired,
  slides: PropTypes.array,
  fetchSlides: PropTypes.func,
};

const mapStateToAnimalProps = (state) => ({
  slides: state.getIn(['Slides', 'animals'], []),
  typ: 'animals',
  edittingTyp: state.getIn(['editting', 'edittingTyp'], ''),
  edittingIndex: state.getIn(['editting', 'edittingIndex'], ''),
  edittingId: state.getIn(['editting', 'edittingId'], '')
});

const mapStateToTreeProps = (state) => ({
  slides: state.getIn(['Slides', 'trees'], []),
  typ: 'trees',
  edittingTyp: state.getIn(['editting', 'edittingTyp'], ''),
  edittingIndex: state.getIn(['editting', 'edittingIndex'], ''),
  edittingId: state.getIn(['editting', 'edittingId'], '')
});

const Animal = connect(mapStateToAnimalProps, {fetchSlides})(Container);
const Tree = connect(mapStateToTreeProps, {fetchSlides})(Container);

export {Animal, Tree};

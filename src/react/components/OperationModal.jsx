/*
This Component is used in Slideshow, in order to show 
the dialogs after selecting a operation, and in oder to 
emit signals of "cancel" or "submit" to redux store then to
sagas for executing the operation.
*/

import React, {useState, useCallback} from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button'
import {connect} from 'react-redux';
import {cancelEditting} from '../actions';

const OperationModal = (props) => {
    const {slide, typ, cancelEditting} = props;
    const [show, setShow] = useState(true);
    const handleClose = () => setShow(false);
    const onCancel = useCallback(() => {
        cancelEditting();
        handleClose();
    },[cancelEditting, handleClose]);


    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{typ}</Modal.Title>
            </Modal.Header>
            <Modal.Body>Demo! The Operation is not completed</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onCancel}>
                Cancel
                </Button>
                <Button variant="primary" onClick={onCancel}>
                {typ === "delete" ? 'Confirm' : 'Submit'}
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

OperationModal.propTypes = {
    slide: PropTypes.object,
    typ: PropTypes.string,
    cancelEditting: PropTypes.func
};

export default connect(null, {cancelEditting})(OperationModal);
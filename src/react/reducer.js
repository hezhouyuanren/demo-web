import Immutable from "immutable";
import {ACTIONS} from './constants';

export default function reducer(state = Immutable.Map(), action) {
    switch (action.type) {
        case ACTIONS.GOT_SLIDES: {
            return state.setIn(['Slides',action.payload.typ], action.payload.slides);
        }
        case ACTIONS.DELETE_SLIDE: {
            return state.set('editting', action.payload)
                .setIn(['editting', 'edittingTyp'], 'delete');
        }
        case ACTIONS.MODIFY_SLIDE: {
            return state.set('editting', action.payload)
                .setIn(['editting', 'edittingTyp'], 'modify');
        }
        case ACTIONS.ADD_SLIDE: {
            return state.set('editting', action.payload)
                .setIn(['editting', 'edittingTyp'], 'add');
        }

        case ACTIONS.CANCEL_EDITTING: {
            return state.set('editting', '');
        }

    }
    return state;
}


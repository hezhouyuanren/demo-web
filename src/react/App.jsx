import React from "react";
import {Switch, Route,  NavLink} from 'react-router-dom';
import Home from './components/Home';
import {Tree} from './components/Container';

export default function(props) {
  return (
    <div className='main-container'>
      <div className='header'>
        <div className='header-title'>
          <img src='logo.svg' />
        </div>

        <div className='header-navigation'>
          <div className='nav-item'>
            <NavLink to="/">Animal</NavLink>
          </div>
          <div className='nav-item'>
            <NavLink to="/trees">Tree</NavLink>
          </div>
          <div className='nav-item logout'>
            <a href="/logout">Logout</a>
          </div>
        </div>
      </div>

      <div className='content-container'>
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route path='/trees'>
            <Tree />
          </Route>
        </Switch>
      </div>

      <div className='footer'>
        <div className='footer-item'>Cookies</div>
        <div className='footer-item'>Impressum</div>
        <div className='footer-item'>AGB</div>
        <div className='footer-item'>Datenschutzhinweise</div>
      </div>

    </div>

  );
}
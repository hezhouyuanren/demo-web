/*
This is the entrypoint of this react application. Redux, Sagas and
Router are hier enabled to use.
*/

import "regenerator-runtime/runtime";
import React from "react";
import {hydrate} from "react-dom";
import './styles.scss';
import App from "./App";
import reducer from "./reducer";
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux';
import { composeWithDevTools } from "redux-devtools-extension";
import Immutable from 'immutable';
import createSagaMiddleware from 'redux-saga';
import mySaga from './sagas';
import {BrowserRouter} from 'react-router-dom';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer, 
    Immutable.Map({}),
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(mySaga);

export {store};

hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, 
    document.getElementById("root")
);
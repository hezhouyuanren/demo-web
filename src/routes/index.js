import express from "express";
import React from "react";
import {renderToString} from "react-dom/server"
import App from "../react/App";
import reducer from "../react/reducer";
import {createStore} from "redux";
import {Provider} from 'react-redux';
import Immutable from 'immutable';
import {StaticRouter} from 'react-router-dom';
import User from '../models/User';
import passport from 'passport';
import '../handlers/passport';

const router = express.Router();
let warning = '';
let Slides = [
    {
        id: 1,
        typ: 'animal',
        title: 'Lorem ipsum ',
        text: 'consectetuer adipiscing elit. Aenean commodo ',
        img: 'images/animal1.jpg',
    },
    {
        id: 2,
        typ: 'animal',
        title: 'Nullam dictum',
        text: 'Vivamus elementum semper nisi.',
        img: 'images/animal2.jpg',
    },
    {
        id: 3,
        typ: 'animal',
        title: 'Maecena',
        text: '',
        img: 'images/animal3.jpg',
    },
    {
        id: 4,
        typ: 'animal',
        title: '',
        text: ' Vivamus elementum semper nisi',
        img: 'images/animal4.jpg',
    },
    {
        id: 5,
        typ: 'tree',
        title: 'Lorem ipsum ',
        text: 'consectetuer adipiscing elit. Aenean commodo ',
        img: 'images/tree1.jpg',
    },
    {
        id: 6,
        typ: 'tree',
        title: 'Nullam dictum',
        text: 'Vivamus elementum semper nisi.',
        img: 'images/tree2.jpg',
    },
    {
        id: 7,
        typ: 'tree',
        title: 'Maecena',
        text: '',
        img: 'images/tree2.jpg',
    }
]

router.get(['/', '/trees'], async (req, res) => {
    if (req.isAuthenticated()) {
        const context = {};
        const store = createStore(
            reducer, 
            Immutable.Map({ })
        );
        const reactComp = renderToString(
            <Provider store={store}>
                <StaticRouter location={req.url} context={context}>
                    <App />
                </StaticRouter>
            </Provider>
        ); 
        
        res.setHeader('Set-Cookie', `csrfToken=${res.locals.csrftoken}`);
        res.status(200).render('pages/index', {
            reactApp: reactComp
        });
    } else {
        res.redirect(301, '/login');
    }
});

router.get('/login', (req, res) => {
    res.status(200).render('pages/login', {
        csrfToken: res.locals.csrftoken,
        warning: warning
    });
});

router.get('/register', (req, res) => {
    res.status(200).render('pages/register', {
        csrfToken: res.locals.csrftoken,
        warning: warning
    });
});

router.post('/register', (req, res) => {
    warning = '';
    const user = new User({username: req.body.username});
    User.register(user, req.body.password, (err) => {
        if (err) {
            res.redirect(301, '/register');
        } else {
            warning = 'you have successfully registered, please login';
            res.redirect(301, '/login');
        }
    });
});

router.post('/login', (req, res) => {
    warning = '';
    if(!req.body.username) {
        warning = "You have not entered your user name";
        res.redirect(301, '/login');
    } else {
        if(!req.body.password) {
            warning = "You have not entered your password";
            res.redirect(301, '/login');
        } else {
            passport.authenticate('local', (err, user, info) => {
                if (err) {
                    warning = "please try again later";
                    res.redirect(301, '/login');
                }
                if (!user) {
                    res.locals.warning = "You have not registed jet";
                    res.redirect(301, '/login');
                }
                req.logIn(user, (err) => {
                    if (err) {return res.json({login: 'failed', err: err});}
                    res.redirect(301, '/');
                });
            })(req, res);
        }
    }
});

router.get('/logout', (req, res) => {
    req.logout()
    res.locals.warning = "You have loged out";
    res.redirect(301, '/login');
})

router.get('/slides/:typ', (req, res) => {
    if (req.isAuthenticated()) {
        if (req.params.typ === 'animals') {
            res.json(Slides.filter(item => item.typ === 'animal'));
        } else if (req.params.typ === 'trees') {
            res.json(Slides.filter(item => item.typ === 'tree'));
        }
    } 
    
    else {
        res.status(404).json([])
    }
})


export default router;
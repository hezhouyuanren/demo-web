const path = require("path");

const config = {
    entry: {
        vendor: ["@babel/polyfill", "react", "hls.js"], // Third party libraries
        //vendor: ["@babel/polyfill", "react"],
        index: ["./src/react/index.jsx"],
        /// Every pages entry point should be mentioned here
    },
    output: {
        path: path.resolve(__dirname, "public"), //destination for bundled output is under ./src/public
        filename: "[name].js" // names of the bundled file will be name of the entry files (mentioned above)
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: "babel-loader", // asks bundler to use babel loader to transpile es2015 code
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"] 
                    }
                },
                exclude: [/node_modules/, /public/]
            },
            {
                test: /\.s[ac]ss$/i,
                use: ["style-loader", "css-loader", "sass-loader"]
                //loader: 'css/locals?module&localIdentName=[name]__[local]___[hash:base64:5]'
            },
            { 
                test: /\.jpg$/, 
                loader: "url-loader?mimetype=image/jpg" 
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".wasm", ".mjs", "*"]
    } // If multiple files share the same name but have different extensions, webpack will resolve the one with the extension listed first in the array and skip the rest.
};

module.exports = config;